using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace MindBox.Shapes.Tests
{
    public class ShapesTests
    {
        [Fact]
        public void TriangleCtor_InvalidArguments_ThrowsExceptions()
        {
            Action act = () =>
            {
                var x = new Triangle(-1, 1, 1);
            };

            act.Should()
                .Throw<ArgumentException>()
                .WithMessage("aSide should be greater than zero");

            act = () =>
            {
                var x = new Triangle(1, 0, 1);
            };

            act.Should()
                .Throw<ArgumentException>()
                .WithMessage("bSide should be greater than zero");

            act = () =>
            {
                var x = new Triangle(1, 2, -1);
            };

            act.Should()
                .Throw<ArgumentException>()
                .WithMessage("cSide should be greater than zero");
        }

        [Fact]
        public void TriangleCtor_ImpossibleTriangle_ThrowsException()
        {
            Action act = () =>
            {
                var x = new Triangle(2, 2, 10);
            };

            act.Should()
                .Throw<ArgumentException>()
                .WithMessage("Impossible triangle with such sides 2, 2, 10");
        }

        [Fact]
        public void TriangleCtor_RightTriangle_IsRightTriangleTrue()
        {
            var triangle = new Triangle(3, 4, 5);
            triangle.IsRightTriangle.Should().BeTrue();
        }

        [Fact]
        public void Triangle_CalculateSquare_HappyPath()
        {
            var triangle = new Triangle(3, 4, 5);
            var square = triangle.CalculateSquare();
            square.Should().Be(6);
        }

        [Fact]
        public void CircleCtor_invalidRadius_ThrowException()
        {
            var circle = new Circle(3);
            var square = circle.CalculateSquare();
            square.Should().Be(28.274333882308138);
        }

        [Fact]
        public void Circle_CalculateSquare_HappyPath()
        {
            Action act = () =>
            {
                var x = new Circle(-1);
            };

            act.Should()
                .Throw<ArgumentException>()
                .WithMessage("radius should be greater than zero");
        }


        [Fact]
        public void IShape_Implementations_CalculateSquare_HappyPath()
        {
            var list = new IShape[]
            {
                new Triangle(3, 4, 5),
                new Circle(3)
            };

            var squares = list.Select(s => s.CalculateSquare()).ToArray();
            squares.Should().BeEquivalentTo(new[] {6, 28.274333882308138});
        }
    }
}