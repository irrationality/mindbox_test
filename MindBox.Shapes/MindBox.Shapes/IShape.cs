namespace MindBox.Shapes
{
    //if you need to add new shapes just implement this interface
    public interface IShape
    {
        double CalculateSquare();  
        
        //todo add another methods if needed
    }
}