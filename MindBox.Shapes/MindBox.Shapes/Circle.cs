using System;

namespace MindBox.Shapes
{
    public class Circle : IShape
    {
        public double Radius { get; }

        public Circle(double radius)
        {
            if (radius <= 0)
            {
                throw new ArgumentException("radius should be greater than zero");
            }

            Radius = radius;
        }

        public virtual double CalculateSquare() => Math.PI * Radius * Radius;
    }
}