using System;

namespace MindBox.Shapes
{
    public class Triangle : IShape
    {
        private const double TOLERANCE = 0.00001;
        public double ASide { get; }
        public double BSide { get; }
        public double CSide { get; }

        public bool IsRightTriangle { get; }

        public Triangle(double aSide, double bSide, double cSide)
        {
            if (aSide <= 0)
            {
                throw new ArgumentException("aSide should be greater than zero");
            }

            if (bSide <= 0)
            {
                throw new ArgumentException("bSide should be greater than zero");
            }

            if (cSide <= 0)
            {
                throw new ArgumentException("cSide should be greater than zero");
            }

            if (aSide + bSide <= cSide ||
                bSide + cSide <= aSide ||
                cSide + aSide <= bSide)
            {
                throw new ArgumentException($"Impossible triangle with such sides {aSide}, {bSide}, {cSide}");
            }


            if (Math.Abs(aSide * aSide + bSide * bSide - cSide * cSide) < TOLERANCE ||
                Math.Abs(bSide * bSide + cSide * cSide - aSide * aSide) < TOLERANCE ||
                Math.Abs(cSide * cSide + aSide * aSide - bSide * bSide) < TOLERANCE)
            {
                IsRightTriangle = true;
            }

            ASide = aSide;
            BSide = bSide;
            CSide = cSide;
        }

        public virtual double CalculateSquare()
        {
            var p = (ASide + BSide + CSide) / 2;
            var square = Math.Sqrt(p * (p - ASide) * (p - BSide) * (p - CSide));
            return square;
        }
    }
}